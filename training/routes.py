def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('login', '/login')
    config.add_route('activateaccount', '/activate')
    config.add_route('logout', '/logout')
    config.add_route('dashboard', '/dashboard')
    config.add_route('settings', '/settings')
    config.add_route('resetpassword', '/resetpassword')

    config.add_route('showone', '/view/{what}/{id:\d+}')
    config.add_route('showall', '/view/{what}')

    config.add_route('cloneobject', '/clone/{courseid:\d+}/{parent:\d+}/{id:\d+}')
    
    config.add_route('new', '/new/{what}')
    config.add_route('edit', '/edit/{what}/{id:\d+}')
    config.add_route('delete', '/delete/{what}/{id:\d+}')
    config.add_route('about', '/about')

    #for course modification
    config.add_route('overview', '/overview/{courseid:\d+}')
    config.add_route('modifycourse', '/modify/{courseid:\d+}')
    config.add_route('courseaction', '/mod/{courseid:\d+}/*args')
    
    #subscriptions
    config.add_route('subscradd', '/subscribe/{courseid}')
    config.add_route('subscrpause', '/sub/{action}/{subscrid:\d+}')
    config.add_route('payment', '/payment/{action}/{courseid:\d+}/{userid:\d+}')
    config.add_route('paymentcheck', '/check')
    config.add_route('checkdiscount', '/checkdiscount')

    #workouts
    config.add_route('workout', '/wkt/{wktid:\d+}')
    
    #terms and conditions
    config.add_route('terms', '/terms')
    
    #shoes sizes
    config.add_route('sizes', '/sizes')

    config.add_route('saveresults', '/saveresults')
    config.add_route('sendemails', '/sendemails')
