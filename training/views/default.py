# -*- coding: utf-8

import hashlib
import re
import smtplib
import datetime
import random
import transaction
import json
import os
import pandas as pd
from pandas.api.types import CategoricalDtype
import base64
from decimal import Decimal, ROUND_HALF_UP
from functools import wraps

from plotly import tools
import plotly.plotly as py
from plotly.offline import plot
import plotly.graph_objs as go

import numpy as np
import email.mime.multipart
from cStringIO import StringIO
from email.mime.text import MIMEText
from pyramid.response import Response
from pyramid.view import view_config
from pyramid.security import (
    authenticated_userid,
    forget,
    remember
)

from pyramid.events import subscriber
from pyramid.events import BeforeRender

from pyramid.httpexceptions import (
    HTTPForbidden,
    HTTPFound,
    HTTPNotFound,
    HTTPSeeOther
)

from pyramid.i18n import (
    TranslationStringFactory,
    get_localizer,
    get_locale_name
)

from sqlalchemy.exc import DBAPIError
from sqlalchemy.sql import and_, or_, not_
from sqlalchemy.orm.session import make_transient 

from ..models import User, CourseElement, Media, Subscription, Payment, Article, SubscrType, Grade, Example, Discount, Result

re_int = re.compile("\d+\.?\d*")

#auth_decorator
#def login_required(func):
#    @wraps(func)
#    def new_func(request):
#        if(request.cookies.get('user')): 
#            return func(request)
#
#        else:
#            return Response('not authirised')
#    return new_func


gripdict = {
    u"открытый":"open.jpg", 
    u"полузакрытый":"half.jpg",
    u"закрытый":"closed.jpg",
    u"пассив":"passive.jpg",
    u"щипок":"pinch.jpg"
    }

@subscriber(BeforeRender)
def add_global(event):
    # заполняем глобальные переменные, чтобы 
    # не передавать их в каждый шаблон
    request = event.get('request')
    cfg = request.registry.settings
    mailto = cfg.get('email')
    phone = cfg.get('tel')
    minprice = cfg.get('minprice')
    event['minprice'] = minprice
    whatsappphone = cfg.get('whatsapp')
    event['phone'] = phone
    event['email'] = mailto
    event['whatsappphone'] = whatsappphone
    # EXCLUDE FREE WEEK
    courses = request.dbsession.query(CourseElement).filter(CourseElement.parent_id == None).filter(CourseElement.is_published == 1).filter(not_(CourseElement.is_archived)).filter(CourseElement.price.has(SubscrType.price != 0)).order_by(CourseElement.name).all()
    authors = request.dbsession.query(User).filter(or_(User.is_author == 1, User.is_admin==1)).order_by(User.is_admin.desc()).all()
    event['authors'] = authors
    event['courses'] = courses
    if authenticated_userid(request):
        user = request.dbsession.query(User).filter(User.login == authenticated_userid(request)).first()
        event['authuser'] = user


@view_config(route_name='cloneobject')
def cloneelement(request):
    # parent - это куда копируем
    # id - что копируем
    # берем элемент по id, вытаскиваем его, копируем с новым parent id
    # и перекидываем на modifycourse

    courseid = int(request.matchdict['courseid'])
    sourceid = int(request.matchdict['id'])
    copytoid = int(request.matchdict['parent'])
    
    parentelement = request.dbsession.query(CourseElement).filter(CourseElement.id == copytoid).first()
    sourceelement = request.dbsession.query(CourseElement).filter(CourseElement.id == sourceid).first()

    # cloning examples
    if parentelement.elemtype == 'exc':
        sourceelement = request.dbsession.query(Example).filter(Example.id == sourceid).first()

    walkthetree(request.dbsession, sourceelement, copytoid)

    return HTTPSeeOther(location=request.route_url('modifycourse', courseid=courseid, _anchor=copytoid))

@view_config(route_name='modifycourse', renderer='../templates/coursemodifytemplate.jinja2')
def modify(request):
    if not authenticated_userid(request):
        return HTTPSeeOther(location=request.route_url('home'))
    user = request.dbsession.query(User).filter(User.login == authenticated_userid(request)).first()
    if not user.is_author:
        return HTTPSeeOther(location=request.route_url('home'))
    tpl = {}
    cid = int(request.matchdict['courseid'])
    tomodify = request.dbsession.query(CourseElement).filter(CourseElement.id==cid).first()
    allcourses = request.dbsession.query(CourseElement).filter(CourseElement.elemtype=='course').all()
    allexamples = request.dbsession.query(Example).distinct(Example.url).group_by(Example.url).all()
    print "*"*80
    print allexamples
    print "*"*80
    tpl.update({'course':tomodify, 'allcourses':allcourses, 'allexamples':allexamples})
    return tpl


@view_config(route_name='courseaction', renderer='../templates/courseaction.jinja2')
def courseaction(request):
    """
    add/delete/modify
    ?week, wkt, exc
    """

    actions = {'new': u"Добавление", 'edit': u"Правка", 'delete': u"Удаление"}
    objs = {'week':u'недели', 'workout':u'дня', 'round':u'части', 'excercise':u'упражнения'}

    if not authenticated_userid(request):
        return HTTPSeeOther(location=request.route_url('home'))

    if request.POST:
        try:
            csrf = request.POST.get('csrf')
            action = request.POST.get('action')
            what = request.POST.get('objtype')
            pid = int(request.POST.get('pid'))
            
        except:
            raise HTTPNotFound()

        if csrf == request.session.get_csrf_token():
            if action == 'new':
                if what == 'week':
                    weekdescr = request.POST.get('weekdescr')
                    weektype = request.POST.get('weektype')
                    elemtype = what
                    corder = request.POST.get('customorder')
                    newelem = CourseElement(parent_id=pid,
                                            elemtype=elemtype,
                                            weektype=weektype,
                                            descr=weekdescr,
                                            customorder=corder
                                            )
                    request.dbsession.add(newelem)
                    request.session.flash({
                            'class' : 'success',
                            'text'  : u'Данные сохранены'
                            })
                    if 'submitmore' in request.POST.keys():
                        return HTTPSeeOther(location=request.route_url('courseaction', courseid=pid, args=('new', what, pid)))
                    return HTTPSeeOther(location=request.route_url('modifycourse', courseid=pid, _anchor=newelem.id))

                elif what == 'workout':
                    elem = request.dbsession.query(CourseElement).filter(CourseElement.id==pid).first()
                    wktdescr = request.POST.get('wktdescr')
                    wkttype = request.POST.get('wkttype')
                    elemtype = what
                    totaltime = request.POST.get('wkttotaltime')
                    corder = request.POST.get('customorder')
                    newelem = CourseElement(parent_id=pid,
                                            elemtype='workout',
                                            weektype=wkttype,
                                            descr=wktdescr,
                                            timetotal = totaltime, 
                                            customorder = corder
                                            )
                    request.dbsession.add(newelem)
                    request.session.flash({
                            'class' : 'success',
                            'text'  : u'Данные сохранены'
                            })
                    if 'submitmore' in request.POST.keys():
                        return HTTPSeeOther(location=request.route_url('courseaction', courseid=pid, args=('new', what, pid)))
                    
                    return HTTPSeeOther(location=request.route_url('modifycourse', courseid=elem.parent.id, _anchor=newelem.id))

                elif what == 'round':
                    elem = request.dbsession.query(CourseElement).filter(CourseElement.id==pid).first()
                    roundname = request.POST.get('roundname')
                    rounddescr = request.POST.get('rounddescr')
                    corder = request.POST.get('customorder')
                    newelem = CourseElement(parent_id=pid,
                                            elemtype='round',
                                            name=roundname,
                                            descr=rounddescr,
                                            customorder=corder
                                            )
                    request.dbsession.add(newelem)
                    request.session.flash({
                            'class' : 'success',
                            'text'  : u'Данные сохранены'
                            })
                    if 'submitmore' in request.POST.keys():
                        return HTTPSeeOther(location=request.route_url('courseaction', courseid=elem.parent.parent.id, args=('new', what, pid)))
                    
                    return HTTPSeeOther(location=request.route_url('modifycourse', courseid=elem.parent.parent.id, _anchor=newelem.id))


                elif what == 'excercise':
                    elem = request.dbsession.query(CourseElement).filter(CourseElement.id==pid).first()
                    excdescr = request.POST.get('excdescr')
                    excname = request.POST.get('excname')
                    onerepis = request.POST.get('onerepis')
                    numsets = request.POST.get('numsets')
                    numreps = request.POST.get('numreps')
                    griptype = request.POST.get('griptype')
                    perftime = request.POST.get('perftime')
                    resttime = request.POST.get('resttime')
                    finalrest = request.POST.get('finalrest')
                    corder = request.POST.get('customorder')
                    newelem = CourseElement(parent_id=pid,
                                            elemtype='exc',
                                            name=excname,
                                            descr=excdescr,
                                            onerepis=onerepis,
                                            numsets=numsets,
                                            numreps=numreps,
                                            griptype=griptype,
                                            perftime=perftime,
                                            resttime=resttime,
                                            finalrest=finalrest,
                                            customorder=corder
                                            )
                    request.dbsession.add(newelem)
                    request.session.flash({
                            'class' : 'success',
                            'text'  : u'Данные сохранены'
                            })
                    if 'submitmore' in request.POST.keys():
                        return HTTPSeeOther(location=request.route_url('courseaction', courseid=elem.parent.parent.parent.id, args=('new', what, pid)))

                    return HTTPSeeOther(location=request.route_url('modifycourse', courseid=elem.parent.parent.parent.id, _anchor=newelem.id))

                elif what == 'example':
                    elem = request.dbsession.query(CourseElement).filter(CourseElement.id==pid).first()
                    url = request.POST.get('url')
                    comment = request.POST.get('comment')
                    newexample = Example(parent_id=pid,
                                         url=url,
                                         comment=comment
                                         )
                    request.dbsession.add(newexample)
                    request.session.flash({
                            'class' : 'success',
                            'text'  : u'Данные сохранены'
                            })

                    if 'submitmore' in request.POST.keys():
                        return HTTPSeeOther(location=request.route_url('courseaction', courseid=elem.parent.parent.parent.id, args=('new', what, pid)))
                    
                    return HTTPSeeOther(location=request.route_url('modifycourse', courseid=elem.parent.parent.parent.parent.id, _anchor=pid))


            elif action == 'edit':
                objid = int(request.POST.get('objid'))
                editelem = request.dbsession.query(CourseElement).filter(CourseElement.id==objid).first()
                if what == 'week':
                    weekdescr = request.POST.get('weekdescr')
                    weektype = request.POST.get('weektype')
                    objtype = request.POST.get('objtype')
                    corder = request.POST.get('customorder')
                    if corder == '':
                        corder = None
                    editelem.weektype = weektype
                    editelem.elemtype = objtype
                    editelem.descr = weekdescr
                    editelem.customorder = corder
                    request.dbsession.add(editelem)
                    request.session.flash({
                            'class' : 'success',
                            'text'  : u'Данные сохранены'
                            })

                    return HTTPSeeOther(location=request.route_url('modifycourse', courseid=editelem.parent.id, _anchor=editelem.id))

                elif what == "workout":
                    wktdescr = request.POST.get('wktdescr')
                    wkttype = request.POST.get('wkttype')
                    wkttotaltime = request.POST.get('wkttotaltime')
                    corder = request.POST.get('customorder')
                    if corder == '':
                        corder = None

                    editelem.descr = wktdescr
                    editelem.weektype = wkttype
                    editelem.timetotal = wkttotaltime
                    editelem.customorder = corder
                    request.dbsession.add(editelem)
                    request.session.flash({
                            'class' : 'success',
                            'text'  : u'Данные сохранены'
                            })

                    return HTTPSeeOther(location=request.route_url('modifycourse', courseid=editelem.parent.parent.id, _anchor=editelem.id))
                    
                elif what == "round":
                    rnddescr = request.POST.get('rounddescr')
                    rndname = request.POST.get('roundname')
                    corder = request.POST.get('customorder')
                    if corder == '':
                        corder = None

                    editelem.name = rndname
                    editelem.descr = rnddescr
                    editelem.customorder = corder
                    request.dbsession.add(editelem)
                    request.session.flash({
                            'class' : 'success',
                            'text'  : u'Данные сохранены'
                            })

                    return HTTPSeeOther(location=request.route_url('modifycourse', courseid=editelem.parent.parent.parent.id, _anchor=editelem.id))
                
                elif what == "excercise":
                    excdescr = request.POST.get('excdescr')
                    excname = request.POST.get('excname')
                    onerepis = request.POST.get('onerepis')
                    numsets = request.POST.get('numsets')
                    numreps = request.POST.get('numreps')
                    griptype = request.POST.get('griptype')
                    perftime = request.POST.get('perftime')
                    resttime = request.POST.get('resttime')
                    finalrest = request.POST.get('finalrest')
                    corder = request.POST.get('customorder')
                    if corder == '':
                        corder = None

                    editelem.name = excname
                    editelem.descr = excdescr
                    editelem.onerepis = onerepis
                    editelem.numsets = numsets
                    editelem.numreps = numreps
                    editelem.griptype = griptype
                    editelem.perftime = perftime
                    editelem.resttime = resttime
                    editelem.finalrest = finalrest
                    editelem.customorder = corder
                    request.dbsession.add(editelem)
                    request.session.flash({
                            'class' : 'success',
                            'text'  : u'Данные сохранены'
                            })
                    
                    return HTTPSeeOther(location=request.route_url('modifycourse', courseid=editelem.parent.parent.parent.parent.id, _anchor=editelem.id))
                elif what == 'example':
                    editelem = request.dbsession.query(Example).filter(Example.id==objid).first()
                    exc = editelem.excercise
                    url = request.POST.get('url')
                    comment = request.POST.get('comment')
                    editelem.url = url
                    editelem.comment = comment
                    request.dbsession.add(editelem)
                    request.session.flash({
                            'class' : 'success',
                            'text'  : u'Данные сохранены'
                            })
                    
                    return HTTPSeeOther(location=request.route_url('modifycourse', courseid=exc.parent.parent.parent.parent.id, _anchor=exc.id))

                else:
                    raise HTTPNotFound()
            else:
                raise HTTPNotFound()
    else:
        parentid = request.matchdict['courseid']
        action = request.matchdict['args'][0]
        what = request.matchdict['args'][1]

        if action == 'new':
            parentcourse = request.dbsession.query(CourseElement).filter(CourseElement.id==parentid).first()
            if what == 'week':
                tpl = {'parent': parentcourse, "actions":actions, "act":action, "objs":objs, "what":what, "objparent":parentcourse}
                return tpl

            elif what == 'workout':
                try:
                    whatid = int(request.matchdict['args'][2])
                except:
                    raise HTTPNotFound()

                parentcourse = request.dbsession.query(CourseElement).filter(CourseElement.id==parentid).first()
                objparent = request.dbsession.query(CourseElement).filter(CourseElement.id==whatid).first()
                tpl = {'parent': parentcourse, "actions":actions, "act":action, "objs":objs, "what":what, "objparent":objparent}
                return tpl

            elif what == 'round':
                try:
                    whatid = int(request.matchdict['args'][2])
                except:
                    raise HTTPNotFound()

                parentcourse = request.dbsession.query(CourseElement).filter(CourseElement.id==parentid).first()
                objparent = request.dbsession.query(CourseElement).filter(CourseElement.id==whatid).first()
                tpl = {'parent': parentcourse, "actions":actions, "act":action, "objs":objs, "what":what, "objparent":objparent}
                return tpl
            
            elif what == 'excercise':
                try:
                    whatid = int(request.matchdict['args'][2])
                except:
                    raise HTTPNotFound()
                
                parentcourse = request.dbsession.query(CourseElement).filter(CourseElement.id==parentid).first()
                objparent = request.dbsession.query(CourseElement).filter(CourseElement.id==whatid).first()
                tpl = {'parent': parentcourse, "actions":actions, "act":action, "objs":objs, "what":what, "objparent":objparent}

                return tpl

            elif what == 'example':
                try:
                    whatid = int(request.matchdict['args'][2])
                except:
                    raise HTTPNotFound()

                parentcourse = request.dbsession.query(CourseElement).filter(CourseElement.id==parentid).first()
                objparent = request.dbsession.query(CourseElement).filter(CourseElement.id==whatid).first()
                tpl = {'parent': parentcourse, "actions":actions, "act":action, "objs":objs, "what":what, "objparent":objparent}

                return tpl

            else:
                raise HTTPNotFound()

        elif action == 'edit':
            parentcourse = request.dbsession.query(CourseElement).filter(CourseElement.id==parentid).first()

            if what == 'week':
                try:
                    whatid = int(request.matchdict['args'][2])
                except:
                    raise HTTPNotFound()
                obj = request.dbsession.query(CourseElement).filter(CourseElement.id==whatid).first()
                tpl = {'parent': parentcourse, "objparent":obj.parent, "actions":actions, "act":action, "objs":objs, "what":what, "obj":obj}
                return tpl
            elif what == "workout":
                try:
                    whatid = int(request.matchdict['args'][2])

                except:
                    raise HTTPNotFound()
                obj = request.dbsession.query(CourseElement).filter(CourseElement.id==whatid).first()
                tpl = {'parent': parentcourse, "objparent":obj.parent, "actions":actions, "act":action, "objs":objs, "what":what, "obj":obj}
                return tpl

            elif what == "round":
                try:
                    whatid = int(request.matchdict['args'][2])

                except:
                    raise HTTPNotFound()
                obj = request.dbsession.query(CourseElement).filter(CourseElement.id==whatid).first()
                tpl = {'parent': parentcourse, "objparent":obj.parent, "actions":actions, "act":action, "objs":objs, "what":what, "obj":obj}
                return tpl
            elif what == "excercise":
                try:
                    whatid = int(request.matchdict['args'][2])
                    
                except:
                    raise HTTPNotFound()
                obj = request.dbsession.query(CourseElement).filter(CourseElement.id==whatid).first()
                tpl = {'parent': parentcourse, "objparent":obj.parent, "actions":actions, "act":action, "objs":objs, "what":what, "obj":obj}
                return tpl
            elif what == "example":
                try:
                    whatid = int(request.matchdict['args'][2])
                except:
                    raise HTTPNotFound()

                obj = request.dbsession.query(Example).filter(Example.id==whatid).first()
                tpl = {'parent': parentcourse, "objparent":obj.excercise, "actions":actions, "act":action, "objs":objs, "what":what, "obj":obj}
                return tpl

        elif action == 'delete':
            whatid = int(request.matchdict['args'][2])
            if what == 'example':
                todelete = request.dbsession.query(Example).filter(Example.id==whatid).first()
                pid = todelete.parent_id
            else:
                todelete = request.dbsession.query(CourseElement).filter(CourseElement.id==whatid).first()
                pid = todelete.parent.id
            request.dbsession.delete(todelete)
            request.session.flash({
                    'class' : 'success',
                    'text'  : u'Данные удалены'
                    })
            return HTTPSeeOther(location=request.route_url('modifycourse', courseid=parentid, _anchor=pid))
        else:
            raise HTTPNotFound()

@view_config(route_name='showone', renderer='../templates/articletemplate.jinja2')
def showoneview(request):
    tpl = {}
    objid = request.matchdict['id']
    what = request.matchdict['what']
    cfg = request.registry.settings
    minprice = cfg.get('minprice')

    if what == 'article':
        q = request.dbsession.query(Article)
        obj = Article
    if what == 'course':
        q = request.dbsession.query(CourseElement).filter(CourseElement.parent_id == None)
        obj = CourseElement
    
    if not authenticated_userid(request):
        res = q.filter(obj.id==objid).filter(obj.is_published==1).filter(not_(obj.is_archived)).first()
    else:
        user = request.dbsession.query(User).filter(User.login == authenticated_userid(request)).first()
        res = q.filter(obj.id==objid).filter(obj.is_published==1).filter(not_(obj.is_archived)).first()

        if user.is_author:
            res = q.filter(obj.id==objid).first()
        if user.is_admin:
            res = q.filter(obj.id==objid).first()

    if res is None:
        raise HTTPNotFound()
    if what == 'course':
        tpl.update({"minprice":minprice})
        try:
            if user and not user.is_admin:
                if res.price is not None and res.price.price == 0:
                    request.session.flash({
                            'class' : 'info',
                            'text'  : u'Бесплатная неделя закончилась, пришло время попробовать себя в деле!'
                            })
                    return HTTPSeeOther(location=request.route_url('showall', what=what+'s'))
        except NameError as e:
            if res.price.price == 0:
                request.session.flash({
                        'class' : 'info',
                        'text'  : u'Бесплатная неделя закончилась, пришло время попробовать себя в деле!'
                        })
                return HTTPSeeOther(location=request.route_url('showall', what=what+'s'))

    tpl.update({'obj':res})
    

    return tpl

@view_config(route_name='overview', renderer='../templates/overviewtemplate.jinja2')
def overviewview(request):
    tpl = {}
    user = request.dbsession.query(User).filter(User.login == authenticated_userid(request)).first()
    if not user.is_admin or not user.is_author:
        return HTTPSeeOther(location=request.route_url('home'))

    # fill empty examples 
    #allex = request.dbsession.query(CourseElement).distinct(CourseElement.name).filter(CourseElement.elemtype == 'exc').filter(CourseElement.examples == None).all()

    #exs = []
    #for a in allex:
    #    print "*"*80
    #    print a.id, a.name
    #    print "*"*80
        #if a.name == u"Вис на одной руке":
        #    newexample = Example(parent_id=a.id, url="https://www.youtube.com/embed/nCHI-RZmKIk")
        #    request.dbsession.add(newexample)
        #    request.dbsession.flush()

            
    #        for ex in a.examples:
    #            if "youtube" in ex.url or "vimeo" in ex.url:
    #                request.dbsession.delete(ex)

        # для каждого имени находим непустой пример,
        # все пустые примеры
        # берем непустой пример, копируем его в пустые
        #withex = request.dbsession.query(CourseElement).filter(CourseElement.elemtype == 'exc').filter(CourseElement.name==a.name).filter(CourseElement.examples.any(Example.url.like("https%"))).first()
        #if withex is not None:
        #    withoutex = request.dbsession.query(CourseElement).filter(CourseElement.elemtype == 'exc').filter(CourseElement.name==a.name).filter(CourseElement.id !=withex.id).all()
        #
        #
        #    for w in withoutex:
        #        for e in withex.examples:
        #            if "youtube" in e.url or "vimeo" in e.url:
        #                newexample = Example(parent_id=w.id, url=e.url)
        #                request.dbsession.add(newexample)
        #                request.dbsession.flush()


        #withjpgonly = request.dbsession.query(CourseElement).filter(CourseElement.elemtype == 'exc').filter(CourseElement.name==a.name).filter(CourseElement.examples.any(Example.url.like("%.jpg"))).all()



            #if exampleurl is not None:
                #newexample = Example(parent_id=w.id, url=exampleurl)
                #request.dbsession.add(newexample)
                #request.dbsession.flush()
    # выбираем distinct все непустые примеры.
    # для каждого смотрим родителя,
    # берем его имя и находим все упражнения с таким именем
    # и для каждого найденного упражнения создаем новый пример как копию
    
    

    #update grip examples
    #allex = request.dbsession.query(CourseElement).filter(CourseElement.elemtype == 'exc').all()
    #for ex in allex:
    #    if ex.griptype == u'открытый':
    #        newexample = Example(parent_id=ex.id, url="/static/grips/open.jpg")
    #        request.dbsession.add(newexample)
    #        request.dbsession.flush()

    #    elif ex.griptype == u'полузакрытый':    
    #        newexample = Example(parent_id=ex.id, url="/static/grips/half.jpg")
    #        request.dbsession.add(newexample)
    #        request.dbsession.flush()

    #    elif ex.griptype == u'закрытый':
    #        newexample = Example(parent_id=ex.id, url="/static/grips/closed.jpg")
    #        request.dbsession.add(newexample)
    #        request.dbsession.flush()

    #    elif ex.griptype == u'пассив':    
    #        newexample = Example(parent_id=ex.id, url="/static/grips/passive.jpg")
    #        request.dbsession.add(newexample)
    #        request.dbsession.flush()

    #    elif ex.griptype == u'щипок':
    #        newexample = Example(parent_id=ex.id, url="/static/grips/pinch.jpg")
    #        request.dbsession.add(newexample)
    #        request.dbsession.flush()
            

    # generate 200 discount codes with same value
    #codes = []
    #amount = 99
    #for r in xrange(5):
    #    newcode = Discount()
    #    code = ''.join(random.choice('0123456789ABCDEF') for i in range(6))
    #    if code not in codes:
    #        codes.append(code)
    #        newcode.code = code
    #        newcode.amount = amount
    #        newcode.descr = u"-99%!"
    #        request.dbsession.add(newcode)

    # generate 200 discount codes
    # with same value
    #codes = []
    #amount = 20
    #for r in xrange(200):
    #    newcode = Discount()
    # #   # #''.join(random.choice('0123456789ABCDEF') for i in range(6))
    #    code = "SECRETCODE20"
    #    #if code not in codes:
    #    #    codes.append(code)
    #    newcode.code = code
    #    newcode.amount = amount
    #    newcode.descr = u"-20%, Skalaoria"
    #    request.dbsession.add(newcode)



    # update courses grade ONCE!
    #allcourses = request.dbsession.query(CourseElement).filter(CourseElement.parent_id == None).all()
    #grades = request.dbsession.query(Grade)
    #shortrpice = request.dbsession.query(SubscrType).filter(SubscrType.subscrtype == "short").first()
    #for c in allcourses:
    #    cname = c.name.split()[-1].lower()
    #    coursegrade = grades.filter(Grade.grade==cname).first()
    #    print "*"*80
    #    print cname
    #    print coursegrade
    #    print "*"*80
    #    c.grade_id = coursegrade.id
    #    c.price_id = shortprice.id
    #    transaction.commit()
    #    request.dbsession.flush()

    cid = request.matchdict['courseid']
    course = request.dbsession.query(CourseElement).filter(CourseElement.id == cid).first()
    tpl.update({"course":course})
    return tpl


@view_config(route_name='showall', renderer='../templates/articlestemplate.jinja2')
def showallview(request):
    tpl = {}
    what = request.matchdict['what']

    if what == 'articles':
        obj = Article
        q = request.dbsession.query(Article)

    elif what == 'courses':
        obj = CourseElement
        # EXCLUDE FREE WEEK
        q = request.dbsession.query(CourseElement).filter(CourseElement.parent_id == None).filter(CourseElement.price.has(SubscrType.price != 0))
    else:
        obj = Article
        q = request.dbsession.query(Article)
        
    if not authenticated_userid(request):
        res = q.filter(obj.is_published==1).filter(not_(obj.is_archived))

    else:
        user = request.dbsession.query(User).filter(User.login == authenticated_userid(request)).first()
        res = q.filter(obj.is_published==1).filter(not_(obj.is_archived))

        if user.is_author:
            res = q#.filter(not_(obj.is_archived))
        if user.is_admin:
            res = q

    if what == 'courses':
        res = res.order_by(obj.name, obj.customorder).all()
        tpl.update({'what':'courses'})
    if what == 'articles':
        res = res.all()
        tpl.update({'what':'articles'})

    tpl.update({'obj':res})
    return tpl


@view_config(route_name='new', renderer='../templates/newarticlestemplate.jinja2')
def newview(request):
    if not authenticated_userid(request):
        return HTTPSeeOther(location=request.route_url('home'))
    cfg = request.registry.settings
    minprice = cfg.get('minprice')
    user = request.dbsession.query(User).filter(User.login == authenticated_userid(request)).first()
    if not user.is_author:
        return HTTPSeeOther(location=request.route_url('articles'))
    
    what = request.matchdict['what']
    tpl = {'what':what}

    if request.POST:
        if what == 'article':
            newobj = Article()
        elif what == 'course':
            newobj = CourseElement()
            
        csrf = request.POST.get('csrf', '')
        if csrf == request.session.get_csrf_token():
            newobj.author_id = user.id
            newobj.headerimage = request.POST.get('headimg')
          
            if user.is_admin or user.is_author:
                if 'published' in request.POST.keys():
                    newobj.is_published = 1
                else:
                    newobj.is_published = 0
                if 'archived' in request.POST.keys():
                    newobj.is_archived = 1
                else:
                    newobj.is_archived = 0

            if what == 'course':
                # create new subscrtype
                # add price
                # commit
                # add new price id to course
                newsubscrtype = SubscrType(
                    subscrtype = request.POST.get('long', 'short'), 
                    price = int(request.POST.get('customprice', minprice)),
                    comment = u"Custom price from user %s" % user.login
                    )
                request.dbsession.add(newsubscrtype)
                transaction.commit()
                request.dbsession.flush()

                newobj.name = request.POST.get('head')
                newobj.price_id = newsubscrtype.id
                newobj.coursetype = request.POST.get('long', 'short')
                newobj.descr = request.POST.get('descr')
                newobj.elemtype = what

            elif what == 'article':
                 newobj.header = request.POST.get('head')

            newobj.preview = request.POST.get('preview')
            if what == 'article':
                newobj.body = request.POST.get('newpost')
            request.dbsession.add(newobj)
            request.session.flash({
                    'class' : 'success',
                    'text'  : u'Данные сохранены'
                    })
            return HTTPSeeOther(location=request.route_url('showall', what=what+'s'))
        else:
            request.session.flash({
                    'class' : 'warning',
                    'text'  : u'Что-то пошло не так, данные не сохранились'
                    })
            
            return HTTPSeeOther(location=request.route_url('new', what=what))
    
    return tpl

@view_config(route_name='edit', renderer='../templates/newarticlestemplate.jinja2')
def editview(request):
    if not authenticated_userid(request):
        return HTTPSeeOther(location=request.route_url('home'))
    cfg = request.registry.settings
    minprice = cfg.get('minprice')
    
    user = request.dbsession.query(User).filter(User.login == authenticated_userid(request)).first()
    what = request.matchdict['what']
    objid = request.matchdict['id']

    if not user.is_author and what in ('course', 'article'):
        return HTTPSeeOther(location=request.route_url('showone', what=what, id=objid))

    tpl = {}
    if what == 'article':
        obj = Article
    if what == 'course':
        obj = CourseElement
    if what == 'user':
        obj = User

    tpl.update({'what':what})

    res = request.dbsession.query(obj).filter(obj.id == objid).first()

    if what != 'user':
        if not user.is_admin and res.author != user:
            return HTTPSeeOther(location=request.route_url('showone', what=what, id=objid))

    if what == 'user':
        if res:
            if user.id != res.id:
                if not user.is_admin:
                    request.session.flash({
                            'class' : 'warning',
                            'text'  : u'Что-то тут не так. Возвращаем вас на всякий случай обратно к редактированию'
                            })
                    return HTTPSeeOther(location=request.route_url('edit', what=what, id=user.id))
        else:
            raise HTTPNotFound()

    if request.POST:
        csrf = request.POST.get('csrf', '')
        if csrf == request.session.get_csrf_token():
            # process course/article
            if what in ('course', 'article'):
                if res.author_id == user.id:
                    res.author_id = user.id
                res.headerimage = request.POST.get('headimg')
                if what == 'course':
                    res.name = request.POST.get('head')
                    res.price.price = request.POST.get('customprice', minprice)
                    res.descr = request.POST.get('descr')
                    if request.POST.has_key('long'):
                        res.coursetype = 'long'
                    else:
                        res.coursetype = 'short'

                    res.elemtype = what
                else:
                    res.header = request.POST.get('head')

                res.preview = request.POST.get('preview')

                if user.is_admin or (user.is_author and res.author_id == user.id):
                    if 'published' in request.POST.keys():
                        if len(res.children) == 0:
                            request.session.flash({
                                    'class' : 'warning',
                                    'text'  : u'Мы не можем опубликовать пустой курс, надо добавить хоть что-нибудь.'
                                    })
                            return HTTPSeeOther(location=request.route_url('edit', what=what, id=res.id))

                        res.is_published = 1
                    else:
                        res.is_published = 0
                    if 'archived' in request.POST.keys():
                        res.is_archived = 1
                    else:
                        res.is_archived = 0
                if request.POST.has_key('newpost'):
                    res.body = request.POST.get('newpost')
                res.lastchanged = datetime.datetime.utcnow()

            elif what == "user":
                author = request.POST.get('author', 0)
                if author != 0:
                    author = 1
                res.is_author = author
                admin = request.POST.get('admin', 0)
                if admin != 0:
                    admin = 1
                res.is_admin = admin
                confirmed = request.POST.get('confirmed', 0)
                if confirmed != 0:
                    confirmed = 1
                res.is_confirmed = confirmed
                    
                res.name = request.POST.get('name')
                res.login = request.POST.get('login')
                res.comments = request.POST.get('comments')


            request.dbsession.add(res)
            request.session.flash({
                    'class' : 'success',
                    'text'  : u'Изменения сохранены'
                    })
            if what in ('course', 'article'):
                return HTTPSeeOther(location=request.route_url('showone', what=what, id=objid))
            elif what == 'user':
                return HTTPSeeOther(location=request.route_url('settings'))
                                    
        else:
            request.session.flash({
                    'class' : 'warning',
                    'text'  : u'Что-то пошло не так, не сохранено'
                    })
            
            return HTTPSeeOther(location=request.route_url('edit', what=what, id=objid))

    tpl.update({'obj':res})
    return tpl

@view_config(route_name='delete')
def deleteview(request):
    """
    the one to delete everything!
    """
    if not authenticated_userid(request):
        return HTTPSeeOther(location=request.route_url('home'))

    what = request.matchdict['what']
    objid = int(request.matchdict['id'])

    user = request.dbsession.query(User).filter(User.login == authenticated_userid(request)).first()
    
    if what == 'payment':
        obj = Payment
        if user.is_admin == 0:
            payment = request.dbsession.query(obj).filter(obj.id == objid).filter(obj.user_id == user.id).first()
        else:
            payment = request.dbsession.query(obj).filter(obj.id == objid).first()
        if payment is not None:
            payment.status = 'cancelled'
            request.dbsession.add(payment)
            #request.dbsession.delete(payment)
            #request.dbsession.delete(subscr)
            request.session.flash({
                    'class' : 'success',
                    'text'  : u'Платёж отменен'
                    })
    
            return HTTPSeeOther(location=request.route_url('settings'))


    elif what in ('article', 'course'):
        if not user.is_author:
            return HTTPSeeOther(location=request.route_url('home'))

        if what == 'article':
            obj = Article

        if what == 'course':
            obj = CourseElement

        todelete = request.dbsession.query(obj).filter(obj.id == objid).first()
        todelete.is_archived = 1
        todelete.is_published = 0
        request.dbsession.add(todelete)
        request.session.flash({
                'class' : 'success',
                'text'  : u'Данные отправлены в архив'
                })
    
        return HTTPSeeOther(location=request.route_url('showall', what=what+'s'))


@view_config(route_name='logout')
def logoutview(request):
    if authenticated_userid(request):
        headers = forget(request)
        return HTTPSeeOther(location=request.route_url('home'), headers=headers)
    return HTTPSeeOther(location=request.route_url('login'))

@view_config(route_name='about', renderer='../templates/abouttemplate.jinja2')
def aboutview(request):
    cfg = request.registry.settings
    tpl = {}
    return tpl        
    
@view_config(route_name='login', renderer='../templates/logintemplate.jinja2')
def loginview(request):
    cfg = request.registry.settings
    salt = cfg.get('salt')
    tpl = {}

    redirectto = request.session.get('redirectto', None)
    
    if authenticated_userid(request):
        if redirectto:
            return HTTPSeeOther(location=request.root+redirectto)
        return HTTPSeeOther(location=request.route_url('home'))

    if 'fname' in request.POST:
        fname = request.POST.get('fname', '')
        csrf = request.POST.get('csrf', '')
        login = request.POST.get('login', '')
        password = request.POST.get('password', '')

        if (csrf == request.session.get_csrf_token()) and login:
            if fname == 'login':
                q = request.dbsession.query(User).filter(User.login == login)
                if q.count() == 0:
                    request.session.flash({
                            'class' : 'info',
                            'text'  : u'Попробуйте зарегистрироваться и попробовать войти снова'
                            })
                    return HTTPSeeOther(location=request.route_url('login'))

                for user in q:
                    formpassword = hashlib.md5(salt+password.encode('utf-8')).hexdigest()

                    if user.password == formpassword:
                        if user.is_confirmed == 0:
                            request.session.flash({
                                    'class' : 'warning ',
                                    'text'  : u"""Ваша учетная запись еще не активирована. Проверьте почту, там должно быть письмо для активации. Если его нет, нажмите на <a class="alert-link" href="%s">ссылку</a>, чтобы получить его еще раз""" % request.route_url('activateaccount', _query={'login':user.login, 'sendagain':"y"}) 
                                    })
                            return HTTPSeeOther(location=request.route_url('login'))

                        headers = remember(request, login)
                        request.response.headerlist.extend(headers)
                        if redirectto:
                            del request.session['redirectto']
                            return HTTPSeeOther(location=request.host_url+redirectto, headers=headers)

                        return HTTPFound(request.route_url('home'), headers=headers)

                    request.session.flash({
                            'class' : 'warning',
                            'text'  : u'Неправильный пароль, простите'
                            })
                    return HTTPSeeOther(location=request.route_url('login'))

            elif fname == 'signup':
                # register user
                # проверяем не занят ли логин
                q = request.dbsession.query(User).filter(User.login == login)
                if q.count() > 0:
                    request.session.flash({
                            'class' : 'info',
                            'text'  : u'Похоже, что такой адрес уже использован для регистрации. Вы точно еще не регистрировались?'
                            })
                    return HTTPSeeOther(location=request.route_url('login'))
                else:
                    firstname = request.POST.get('firstname', '')
                    lastname = request.POST.get('lastname', '')
                    uname = "{0} {1}".format(firstname.encode('utf-8'), lastname.encode('utf-8')).decode('utf-8')
                    if len(password) == 0:
                        request.session.flash({
                                'class' : 'info',
                                'text'  : u'Пароль не должен быть пустым'
                                })
                        return HTTPSeeOther(location=request.route_url('login'))

                    newuser = User()
                    newuser.login = login
                    newuser.name = uname
                    newuser.password = hashlib.md5(salt+password.encode('utf-8')).hexdigest()
                    request.dbsession.add(newuser)

                    transaction.commit()
                    request.dbsession.flush()

                    sendmessage(login, 'register', request, newuser.token)
                    #sendmessage('xruninka@gmail.com', 'log', request, mess=u"Новый пользователь на makemestrong.ru, %s" % newuser.login)
                    sendmessage('gontchar@gmail.com', 'log', request, mess=u"Новый пользователь на makemestrong.ru, %s" % newuser.login)
                    request.session.flash({
                            'class' : 'info',
                            'text'  : u'Письмо для активации учетной записи отправлено на %s' % login
                            })
                    

                    return HTTPSeeOther(location=request.route_url('home'))
                    
    return tpl

@view_config(route_name='home', renderer='../templates/hometemplate.jinja2')
def mainview(request):
    tpl = {}
    return tpl

@view_config(route_name='settings', renderer='../templates/settingstemplate.jinja2')
def settingsview(request):
    tpl = {}
    if not authenticated_userid(request):
        return HTTPSeeOther(location=request.route_url('home'))

    user = request.dbsession.query(User).filter(User.login == authenticated_userid(request)).first()
    tpl.update({'authuser':user})

    results = request.dbsession.query(Result).filter(Result.user_id==user.id).order_by(Result.user_id).all()

    #for admins
    if user.is_admin:
        users = request.dbsession.query(User).all()
        courses = request.dbsession.query(CourseElement).filter(CourseElement.parent == None).all()
        subscr = request.dbsession.query(Subscription).all()
        payments = request.dbsession.query(Payment).order_by(Payment.status).all()
        results = request.dbsession.query(Result).order_by(Result.user_id).all()
        tpl.update({'users':users, 'courses':courses, 'subscr':subscr, 'payments':payments})
    
    # for authors
    elif user.is_author and not user.is_admin:
        #get author's courses
        courses = request.dbsession.query(CourseElement).filter(CourseElement.author_id == user.id).all()
        #get all subscriptions for author's courses including author's subsciptions
        subscr = request.dbsession.query(Subscription).join(CourseElement).filter(or_(CourseElement.author_id==user.id, Subscription.user == user.id)).all()
        # get all user payments for author's courses
        payments = request.dbsession.query(Payment).join(Subscription).join(CourseElement).filter(or_(CourseElement.author_id==user.id, Payment.user_id == user.id)).order_by(Payment.status).all()
        # get all users, subscribed to author's courses
        users = request.dbsession.query(User).join(Subscription).filter(Subscription.cname.has(CourseElement.author_id==user.id)).all()
        tpl.update({'courses':courses, 'users':users, 'payments':payments, 'subscr':subscr})

    else:
        # for clients
        subscr = request.dbsession.query(Subscription).filter(Subscription.user == user.id).all()
        payments = request.dbsession.query(Payment).filter(Payment.user_id == user.id).order_by(Payment.status).all()
        tpl.update({'subscr':subscr, 'payments':payments})

    if len(results) > 0:
        res_dict = [{"date":r.date, "pullups":r.pullups, "pushups":r.pushups, "maxgrade":r.maxgrade, "maxhalf":r.maxhalf, "maxopen":r.maxopen, "maxthree":r.maxthree} for r in results]
        categs = []
        for r in range(5, 9):
            for l in ['a', 'b', 'c']:
                for m in ['', '+']:
                    categs.append("{0}{1}{2}".format(r, l, m))
        
        cat_type = CategoricalDtype(categories=categs, ordered=True)
        
        df = pd.DataFrame(res_dict)
        df['maxgrade'] = df['maxgrade'].astype(cat_type)

        df['date'].apply(lambda x: x.strftime('%d%m%Y %H:%M:%S'))
    

        pldata1 = go.Scatter(x=df['date'], y=df['maxgrade'], name='сложность', mode='lines+markers')
        
        pldata2 = [
            go.Scatter(
                x=df['date'], # assign x as the dataframe column 'x'
                y=df['maxhalf'],
                name='полузакрытый', mode='lines+markers'
                ),
            go.Scatter(
                x=df['date'], # assign x as the dataframe column 'x'
                y=df['maxopen'],
                name='открытый', mode='lines+markers'
                ),
            go.Scatter(
                x=df['date'], # assign x as the dataframe column 'x'
                y=df['maxthree'],
                name='три пальца', mode='lines+markers'
                )
            ]

        pldata3 = [
            go.Scatter(
                x=df['date'], # assign x as the dataframe column 'x'
                y=df['pullups'],
                name='подтягивания', mode='lines+markers'
                ),
            go.Scatter(
                x=df['date'], # assign x as the dataframe column 'x'
                y=df['pushups'],
                name='отжимания', mode='lines+markers'
                )
            ]
        
        fig = tools.make_subplots(rows=3, cols=1, subplot_titles=('Максимальная категория', 'Фингерборд', 'ОФП'))

        fig.append_trace(pldata1, 1, 1)
        for d2 in pldata2:
            fig.append_trace(d2, 2, 1)
        for d3 in pldata3:
            fig.append_trace(d3, 3, 1)
    
        plotly_div = plot(fig, output_type='div')
        tpl.update({"plotlyplot":plotly_div})

    return tpl


@view_config(route_name='subscrpause')
def subscrpauseview(request):
    if not authenticated_userid(request):
        request.session.flash({
                'class' : 'success',
                'text'  : u'Войдите или зарегистрируйтесь, чтобы подписаться на курс'
                })
        return HTTPSeeOther(location=request.route_url('login'))
    user = request.dbsession.query(User).filter(User.login == authenticated_userid(request)).first()
    subid = None
    action = None
    mess = u"изменена"
    try:
        subid = int(request.matchdict['subscrid'])
        action = request.matchdict['action']
    except:
        request.session.flash({
                'class' : 'warning',
                'text'  : u'Что-то не так'
                })
        raise HTTPNotFound()
    if subid is not None and action is not None:
        if user.is_admin == 0:
            subscr = request.dbsession.query(Subscription).filter(Subscription.user == user.id).filter(Subscription.id==subid).first()
        else:
            subscr = request.dbsession.query(Subscription).filter(Subscription.id==subid).first()
        if action == "pause":
            subscr.is_paused = 1
            mess = u"приостановлена"
        if action == "unpause":
            if not subscr.is_cancelled == 1:
                subscr.is_paused = 0
                mess = u"возобновлена"
            else:
                mess = u"не может быть возоблена, т.к. она отменена. Нужно подписаться снова"
        if action == "cancel":
            #request.dbsession.delete(subscr.payment)
            #request.dbsession.delete(subscr)
            subscr.is_cancelled = 1
            subscr.is_paused = 1
            mess = u"отменена"
            request.session.flash({
                    'class' : 'success',
                    'text'  : u'Подписка %s' % mess
                    })
            return HTTPSeeOther(location=request.route_url('settings'))

        if action == "restore":
            mess = u"восстановлена"
            subscr.is_cancelled = 0
            if subscr.is_paused == 1:
                subscr.is_paused = 0

        subscr.lastchanged = datetime.datetime.utcnow()
        request.dbsession.add(subscr)
        request.session.flash({
                'class' : 'success',
                'text'  : u'Подписка %s' % mess
                })
        return HTTPSeeOther(location=request.route_url('settings'))

    request.session.flash({
            'class' : 'warning',
            'text'  : u'Что-то не так'
            })
    return HTTPSeeOther(location=request.route_url('dashboard'))


@view_config(route_name='subscradd')
def subscraddview(request):
    tpl = {}
    cid = None
    cfg = request.registry.settings
    minprice = cfg.get('minprice')

    if not authenticated_userid(request):
        request.session['redirectto'] = request.path
        request.session.flash({
                'class' : 'success',
                'text'  : u'Войдите или зарегистрируйтесь, чтобы подписаться на курс'
                })
        return HTTPSeeOther(location=request.route_url('login'))
    try:
        cid = int(request.matchdict['courseid'])
    except:
        request.session.flash({
                'class' : 'warning',
                'text'  : u'Что-то не так'
                })
        raise HTTPNotFound()

    if cid:

        # TODO CUSTOMPRICE pass it to the payment function
        # check for URL hacks for price
        
        course = request.dbsession.query(CourseElement).filter(CourseElement.id == cid).first()
        user = request.dbsession.query(User).filter(User.login == authenticated_userid(request)).first()
        subscr = request.dbsession.query(Subscription).filter(Subscription.user == user.id).filter(Subscription.course_id==cid).first()

        # check if price is set
        #if subscr is None:
        customprice = request.GET.get('pr', float(minprice))
        try:
            customprice = float(customprice)
        except:
            customprice = minprice

        if course.is_published and not course.is_archived:
            if subscr is None:
                # если не подписаны вообще или не оплачено
                # перекидываем на страницу оплаты
                
                if course.price.price == 0:
                    # FREE WEEK
                    # просто добавляем подписку без оплаты
                    #subscrtype = request.dbsession.query(SubscrType).filter(SubscrType.subscrtype == course.coursetype).first()
                    #payment_descr = u"Оплата за подписку на курс %s, %s, %s, БЕСПЛАТНО" % (course.name, user.login, subscrtype.comment)

                    #fl = "status:free"
                    #startdate = datetime.datetime.utcnow()
                    #numweeks = len(course.children)
                    #enddate = startdate + datetime.timedelta(weeks=numweeks) - datetime.timedelta(days=1)
                                
                    #newsubscr = Subscription(user=user.id, course_id=course.id, start=startdate, end=enddate, flags=fl, subscrtype_id=subscrtype.id, is_paid=1, comments="Бесплатная неделя")
                    #request.dbsession.add(newsubscr)
                    #request.dbsession.flush()
                    #newpayment = Payment(user_id=user.id, subscription_id=newsubscr.id, status='confirmed', date=startdate, descr=payment_descr)
                    #request.dbsession.add(newpayment)
                    #request.dbsession.flush()
                    #subscrmess = u"Пользователь %s только что подписался на %s" % (user.login, course.name)
                    #sendmessage('xruninka@gmail.com', 'log', request, mess=subscrmess)
                    #sendmessage('gontchar@gmail.com', 'log', request, mess=subscrmess)
                    request.session.flash({
                            'class' : 'info',
                            'text'  : u'Бесплатная неделя закончилась, пришло время попробовать себя в деле!'
                            })

                    return HTTPSeeOther(location=request.route_url('dashboard'))
                return HTTPSeeOther(location=request.route_url('payment', action="send", courseid=course.id, userid=user.id, _query={"pr":customprice}))

            elif subscr.is_finished == 1:
                request.session.flash({
                        'class' : 'warning',
                        'text'  : u'Ваша подписка на курс %s закончилась %s, если хотите, можете продлить подписку' % (course.name, subscr.end)
                        })
                return HTTPSeeOther(location=request.route_url('payment', action="renew", courseid=course.id, userid=user.id, _query={"sid":subscr.id, "pr":customprice}))
            
            else:
                if subscr.is_paid == 0:
                    return HTTPSeeOther(location=request.route_url('payment', action="send", courseid=course.id, userid=user.id, _query={"pr":customprice}))

                else:
                    request.session.flash({
                            'class' : 'success',
                            'text'  : u'Вы уже подписаны на курс %s' % course.name
                            })
                    
                    return HTTPSeeOther(location=request.route_url('dashboard'))

    request.session.flash({
            'class' : 'warning',
            'text'  : u'Что-то не так'
            })
    raise HTTPNotFound()



@view_config(route_name='paymentcheck')
def paymentcheck(request):
    # тут проверяем платеж
    dbsess = request.dbsession
    sess = request.session
    print "*"*80
    print "PAYMENTCHECK"
    print "*"*80
    request = request.decode('windows-1251')
    result = request.POST.get("WMI_ORDER_STATE")
    
    if result=="Accepted":
        if "discountid" in sess:
            codeid = sess.pop("discountid")
            discount = dbsess.query(Discount).filter(Discount.id == codeid).first()
            dbsess.delete(discount)
            dbsess.flush()

        userid = int(request.POST.get('uid'))
        paymentid = int(request.POST.get('pid'))
        subscrid = int(request.POST.get('sid'))
        courseid = int(request.POST.get('cid'))

        newdescr = request.POST.get('WMI_DESCRIPTION')
        newprice = request.POST.get('WMI_PAYMENT_AMOUNT')

        course = dbsess.query(CourseElement).filter(CourseElement.id == courseid).first()
        payment = dbsess.query(Payment).filter(Payment.id == paymentid).first()
        subscr = dbsess.query(Subscription).filter(Subscription.id == subscrid).first()
        if subscr is not None:
            startdate = datetime.datetime.utcnow()
            numweeks = len(course.children)
            enddate = startdate + datetime.timedelta(weeks=numweeks) - datetime.timedelta(days=1)
            subscr.is_paid = 1
            subscr.lastchanged = startdate
            subscr.end = enddate
            payment.status = "confirmed"
            payment.descr = newdescr + ", %s &#8381;" % newprice
            dbsess.add(subscr)
            dbsess.add(payment)
            transaction.commit()
            #
            #sendmessage('xruninka@gmail.com', 'log', mess=u"Успешная оплата подписки на %s пользователем %s" % (subscr.course.name, subscr.user))
            sendmessage('gontchar@gmail.com', 'log', request, mess=u"Успешная оплата подписки на %s пользователем %s" % (subscr.course.name, subscr.user))
            resp = Response(body='WMI_RESULT=OK', content_type='text/plain')
        else:
            resp = Response(body='WMI_RESULT=OK&WMI_DESCRIPTION=No such subscription, stop trying', content_type='text/plain')
    else:
        resp = Response(body='WMI_RESULT=RETRY&WMI_DESCRIPTION=Something wrong, try again', content_type='text/plain')
        
    return resp

@view_config(route_name='payment', renderer='../templates/paymenttemplate.jinja2')
def paymentview(request):
    # тут отправляем платеж
    tpl = {}
    cfg = request.registry.settings

    if not authenticated_userid(request):
        return HTTPSeeOther(location=request.route_url('login'))

    action = request.matchdict['action']
    cid = int(request.matchdict['courseid'])
    uid = int(request.matchdict['userid'])
    minprice = float(cfg.get('minprice'))

    cfg = request.registry.settings
    merchid = cfg.get('merchantid')
    walleturl = cfg.get('walleturl')
    customprice = request.GET.get('pr', minprice)

    if float(customprice) < minprice:
        customprice = minprice

    user = request.dbsession.query(User).filter(User.login == authenticated_userid(request)).first()
    course = request.dbsession.query(CourseElement).filter(CourseElement.id == cid).first()
    tpl.update({'user':user, 'course':course})
    
    if action == 'send':
        subscrtype = request.dbsession.query(SubscrType).filter(SubscrType.subscrtype == course.coursetype).first()
        payment_descr = u"Оплата за подписку на курс %s, %s, %s" % (course.name, user.login, subscrtype.comment)
        if subscrtype.subscrtype == "short":
            numweeks = len(course.children)
        else:
            numweeks = subscr.subscrtype.subscrtype.numweeks

        # создаем новую подписку только если не подписан или не оплачен
        alreadysubscribed = request.dbsession.query(Subscription).filter(Subscription.user == user.id).filter(Subscription.course_id==course.id).first()
        if alreadysubscribed is None:
            startdate = datetime.datetime.utcnow()
            enddate = startdate + datetime.timedelta(weeks=numweeks) - datetime.timedelta(days=1)
            fl = "status:prod"
            
            newsubscr = Subscription(user=user.id, course_id=course.id, start=startdate, end=enddate, flags=fl, subscrtype_id=subscrtype.id, customprice=customprice)
            request.dbsession.add(newsubscr)
            request.dbsession.flush()
            newpayment = Payment(user_id=user.id, subscription_id=newsubscr.id, status='new', date=startdate, descr=payment_descr)
            request.dbsession.add(newpayment)
            request.dbsession.flush()
            subscrmess = u"Пользователь %s только что подписался на %s" % (user.login, course.name)
            #sendmessage('xruninka@gmail.com', 'log', request, mess=subscrmess)
            sendmessage('gontchar@gmail.com', 'log', request, mess=subscrmess)

            tpl.update({
                    'course': course,
                    'merchid': merchid,
                    'price': customprice,
                    'sid': newsubscr.id,
                    'pid': newpayment.id,
                    'descr': payment_descr,
                    'courseid': cid,
                    'userid': uid,
                    'walleturl': walleturl,
                    })
        else:
            #подписаны, но не оплачено
            if alreadysubscribed.is_paid == 0:
                startdate = datetime.datetime.utcnow()
                enddate = startdate + datetime.timedelta(weeks=numweeks) - datetime.timedelta(days=1)
                alreadysubscribed.start = startdate
                alreadysubscribed.end = enddate
                alreadysubscribed.customprice = customprice
                request.dbsession.add(alreadysubscribed)
                request.dbsession.flush()
                # TODO >> 
                # Создаем только если нет неоплаченных платежей за этот курс
                # если есть неоплаченный платеж за этот курс,
                # вставляем его в платежку
                haspayment = request.dbsession.query(Payment).filter(Payment.subscription_id == alreadysubscribed.id).first()
                if haspayment is None:
                    newpayment = Payment(user_id=user.id, subscription_id=alreadysubscribed.id, status='new', date=startdate, descr=payment_descr)
                else:
                    haspayment.date=startdate
                    haspayment.status = 'new'
                    haspayment.descr = payment_descr
                    newpayment = haspayment

                request.dbsession.add(newpayment)
                request.dbsession.flush()
                    
                subscrmess = u"Пользователь %s только что создал платеж для курса %s" % (user.login, course.name)
                #sendmessage('xruninka@gmail.com', 'log', request, mess=subscrmess)
                sendmessage('gontchar@gmail.com', 'log', request, mess=subscrmess)
                tpl.update({
                        'course': course,
                        'merchid': merchid,
                        'price': alreadysubscribed.customprice,
                        'sid': alreadysubscribed.id,
                        'pid': newpayment.id,
                        'descr': payment_descr,
                        'courseid': cid,
                        'userid': uid,
                        'walleturl': walleturl,
                        })


            else:
                request.session.flash({
                        'class' : 'success',
                        'text'  : u'Вы уже полписаны на этот курс'
                        })
                
                return HTTPSeeOther(location=request.route_url('settings'))

    elif action == 'pay':
        sid = request.GET['sid']
        subscr = request.dbsession.query(Subscription).filter(Subscription.id == sid).first()
        tpl.update({
                'course': course,
                'merchid': merchid,
                'price': customprice,
                'sid': subscr.id,
                'pid': subscr.payment.id,
                'descr': subscr.payment.descr,
                'courseid': cid,
                'userid': uid,
                'walleturl': walleturl,
                })

    elif action == 'renew':
        # подписка закончилась, создаем новый платеж для этой подписки
        sid = request.GET['sid']
        subscr = request.dbsession.query(Subscription).filter(Subscription.id == sid).first()
        payment_descr = u"Продление подписки на курс %s, %s, %s" % (course.name, user.login, subscr.subscrtype.comment)
        startdate = datetime.datetime.utcnow()
        if subscr.subscrtype.subscrtype == "short":
            numweeks = len(course.children)
        else:
            numweeks = subscr.subscrtype.subscrtype.numweeks

        enddate = startdate + datetime.timedelta(weeks=numweeks) - datetime.timedelta(days=1)

        if course.price.price == 0:
            # FREE WEEK
            # просто добавляем подписку без оплаты
            #subscrtype = request.dbsession.query(SubscrType).filter(SubscrType.subscrtype == course.coursetype).first()
            #payment_descr = u"Продление подписки на курс %s, %s, %s, БЕСПЛАТНО" % (course.name, user.login, subscrtype.comment)

            #fl = "status:free"
                                
            #subscr.start=startdate
            #subscr.end=enddate
            #subscr.is_paid=1
            #subscr.comments="Бесплатная неделя"
            #request.dbsession.add(subscr)
            #request.dbsession.flush()
            #newpayment = Payment(user_id=user.id, subscription_id=subscr.id, status='confirmed', date=startdate, descr=payment_descr)
            #request.dbsession.add(newpayment)
            #request.dbsession.flush()
            #subscrmess = u"Пользователь %s только что продлил подписку на %s" % (user.login, course.name)
            #sendmessage('xruninka@gmail.com', 'log', request, mess=subscrmess)
            #sendmessage('gontchar@gmail.com', 'log', request, mess=subscrmess)
            request.session.flash({
                    'class' : 'info',
                    'text'  : u'Бесплатная неделя закончилась, пришло время попробовать себя в деле!'
                    })
            
            return HTTPSeeOther(location=request.route_url('dashboard'))

        subscr.lastchanged = startdate
        subscr.end = enddate
        subscr.customprice = customprice
        request.dbsession.add(subscr)

        newpayment = Payment(user_id=user.id, subscription_id=subscr.id, status='new', date=startdate, descr=payment_descr)
        request.dbsession.add(newpayment)
        request.dbsession.flush()

        tpl.update({
                'course': course,
                'merchid': merchid,
                'price': subscr.customprice,
                'sid': subscr.id,
                'pid': newpayment.id,
                'descr': payment_descr,
                'courseid': cid,
                'userid': uid,
                'walleturl': walleturl,
                })
    
    elif action == 'paycheck':
        sid = request.GET['sid']
        pid = request.GET['pid']
        subscr = request.dbsession.query(Subscription).filter(Subscription.id == sid).first()
        payment = request.dbsession.query(Payment).filter(Payment.id == pid).first()
        course = subscr.course
        if course.price.price == 0:
            # FREE WEEK
            # просто добавляем подписку без оплаты
            #startdate = datetime.datetime.utcnow()
            #if subscr.subscrtype.subscrtype == "short":
            #    numweeks = len(course.children)
            #else:
            #    numweeks = subscr.subscrtype.subscrtype.numweeks

            #enddate = startdate + datetime.timedelta(weeks=numweeks) - datetime.timedelta(days=1)

            #subscrtype = request.dbsession.query(SubscrType).filter(SubscrType.subscrtype == course.coursetype).first()
            #payment_descr = u"Продление подписки на курс %s, %s, %s, БЕСПЛАТНО" % (course.name, user.login, subscrtype.comment)

            #fl = "status:free"
                                
            #subscr.start=startdate
            #subscr.end=enddate
            #subscr.is_paid=1
            #subscr.comments="Бесплатная неделя"
            #request.dbsession.add(subscr)
            #request.dbsession.flush()
            #payment.status='confirmed'
            #payment.date=startdate
            #payment.descr=payment_descr
            #request.dbsession.add(payment)
            #request.dbsession.flush()
            #subscrmess = u"Пользователь %s только что продлил подписку на %s" % (user.login, course.name)
            #sendmessage('xruninka@gmail.com', 'log', request, mess=subscrmess)
            #sendmessage('gontchar@gmail.com', 'log', request, mess=subscrmess)
            #request.session.flash({
            #        'class' : 'success',
            #        'text'  : u'Подписка на курс %s продлена до %s' % (course.name, subscr.end)
            #        })
            request.session.flash({
                    'class' : 'info',
                    'text'  : u'Бесплатная неделя закончилась, пришло время попробовать себя в деле!'
                    })
            
            return HTTPSeeOther(location=request.route_url('dashboard'))
        
        tpl.update({
                'course': course,
                'merchid': merchid,
                'price': customprice,
                'sid': sid,
                'pid': pid,
                'descr': payment.descr,
                'courseid': cid,
                'userid': uid,
                'walleturl': walleturl,
                })
    return tpl

@view_config(route_name='checkdiscount', renderer='json')
def checkdiscount(request):
    code = request.GET.get('code', None)
    if code:
        discount = request.dbsession.query(Discount).filter(Discount.code == code).filter(Discount.descr != "taken").first()
    else: 
        discount = None

    if discount:
        amount = discount.amount
        discount.descr = u"taken"
        request.dbsession.add(discount)
        request.dbsession.flush()
        request.session['discountid'] = discount.id
        amount = discount.amount
    else:
        amount = 0

    return {'content':amount}

@view_config(route_name='workout', renderer='../templates/workouttemplate.jinja2')
def workoutview(request):
    tpl = {}
    if not authenticated_userid(request):
        return HTTPSeeOther(location=request.route_url('login'))
    wktid = int(request.matchdict['wktid'])
    
    user = request.dbsession.query(User).filter(User.login == authenticated_userid(request)).first()
    #get user subscriptions
    workout = request.dbsession.query(CourseElement).filter(CourseElement.id == wktid).filter(CourseElement.elemtype=="workout").first()
    if workout is not None:
        subscr = request.dbsession.query(Subscription).filter(Subscription.user == user.id).filter(Subscription.course_id == workout.parent.parent.id).first()
        if subscr is not None:
            tpl.update({"workout":workout})
            return tpl
    # проверяем подписку на этот курс
    # проверяем чтобы этот воркаут относился к подписанному
    # возвращаем воркаут
    
    raise HTTPNotFound()

@view_config(route_name='dashboard', renderer='../templates/dashboardtemplate.jinja2')
def dashboardview(request):
    tpl = {}
    if not authenticated_userid(request):
        return HTTPSeeOther(location=request.route_url('login'))

    user = request.dbsession.query(User).filter(User.login == authenticated_userid(request)).first()
    subscr = request.dbsession.query(Subscription).filter(Subscription.user == user.id)
    for s in subscr.all():
        if s.is_finished:
            # FREE WEEK
            if s.course.price.price != 0:
                request.session.flash({
                        'class' : 'warning',
                        'text'  : u'Подписка на курс %s закончилась, вы можете продлить её в настройках' % s.course.name
                        })
    tpl.update({'subscr':subscr})
    
    return tpl

@view_config(route_name='activateaccount')
def activateaccountview(request):
    if authenticated_userid(request):
        return HTTPSeeOther(location=request.route_url('home'))

    if 'sendagain' in request.GET.keys() and not 'token' in request.GET.keys():
        #отправляем письмо заново
        login = request.GET.get('login')
        to = login
        fr = 'noreply@manatee.trololo.info'
        replyto = 'a@manatee.trololo.info'
        msg = email.mime.multipart.MIMEMultipart()
        msg['from']=fr
        msg.add_header('reply-to', replyto)
        msg['to']=to
        msg['subject'] = "Регистрация на MakeMeStrong.ru"
        message = """Ура, вы зарегистрировались на makemestrong.ru. 

Для активации учетной записи пройдите по ссылке: {0}

Если это письмо попало к вам по ошибке, просто проигнорируйте эго. 

С уважением, команда MakeMeStrong.
    """
                
        user = request.dbsession.query(User).filter(User.login == login).first()
        if user is None:
            request.session.flash({
                    'class' : 'info',
                    'text'  : u'Такого адреса у нас нет'
                    })
        
            return HTTPSeeOther(location=request.route_url('login'))

        token = user.token
        message = message.format(request.route_url('activateaccount', _query={'login':user.login, 'token':token}))
        msgtext = MIMEText(message, 'plain', 'utf-8')
        msg.attach(msgtext)
        serv = smtplib.SMTP('localhost')
        serv.sendmail(fr, to, msg.as_string())
        serv.quit()
        request.session.flash({
                'class' : 'info',
                'text'  : u'Письмо для активации учетной записи отправлено на %s' % to
                })
        

        return HTTPSeeOther(location=request.route_url('home'))


    if 'token' in request.GET.keys():
        login = request.GET.get('login')
        token = request.GET.get('token')
        user = request.dbsession.query(User).filter(User.login == login).first()
        if user is not None:
            dbtoken  = user.token
            if token != dbtoken:
                request.session.flash({
                        'class' : 'info',
                        'text'  : u'Ваша ссылка устарела или испорчена'
                        })
            else:
                user.is_confirmed = 1
                request.dbsession.add(user)
                request.session.flash({
                        'class' : 'success',
                        'text'  : u'Ваша учетная запись активирована'
                        })
        else:
            request.session.flash({
                    'class' : 'info',
                    'text'  : u'Ваша ссылка устарела или испорчена'
                    })
    
    return HTTPSeeOther(location=request.route_url('login'))


@view_config(route_name='terms', renderer='../templates/termstemplate.jinja2')
def termsview(request):
    return {}

@view_config(route_name='resetpassword', renderer='../templates/resetpasswordtemplate.jinja2')
def resetpasswordview(request):
    tpl = {}

    if authenticated_userid(request):
        return HTTPSeeOther(location=request.route_url('home'))
    if request.POST:
        # третий шаг, сохраняем новый пароль и перезаписываем last_changed
        # if POST, validate params, reset password and redirect to the login page
        login = request.POST.get('login')
        token = request.POST.get('token')
        newpass = request.POST.get('password')
        csrf = request.POST.get('csrf')
        user = request.dbsession.query(User).filter(User.login == login).first()
        dbtoken  = hashlib.md5(user.password+str(user.lastchanged)).hexdigest()
        if token != dbtoken:
            request.session.flash({
                    'class' : 'info',
                    'text'  : u'Ваша ссылка устарела, попробуйте еще раз восстановить пароль'
                    })
            return HTTPSeeOther(location=request.route_url('login'))
        cfg = request.registry.settings
        salt = cfg.get('salt')
        user.password = hashlib.md5(salt+newpass).hexdigest()
        user.lastchanged = datetime.datetime.utcnow()
        request.dbsession.add(user)
        request.session.flash({
                'class' : 'success',
                'text'  : u'Новый пароль сохранен'
                })
        
        return HTTPSeeOther(location=request.route_url('login'))


    elif 'token' in request.GET.keys():
        #второй шаг, приходим по ссылке из email
        # и заполняем новый пароль
        login = request.GET.get('login')
        token = request.GET.get('token')
        # проверка токена
        # !
        user = request.dbsession.query(User).filter(User.login == login).first()
        dbtoken  = hashlib.md5(user.password+str(user.lastchanged)).hexdigest()
        if token != dbtoken:
            request.session.flash({
                    'class' : 'info',
                    'text'  : u'Ваша ссылка устарела, попробуйте еще раз восстановить пароль'
                    })
            return HTTPSeeOther(location=request.route_url('login'))

        # return new password form
        # with hidden params
        tpl.update({'login':user.login,
                    'token':token
                    })
        return tpl
    else:
        #первый шаг, создаем токен
        to = request.GET.get('login')
        user = request.dbsession.query(User).filter(User.login == to.encode('utf-8')).first()
        if user is not None:
            token = user.token
            sendmessage(to, 'reset', request, token)

            print "&"*80
            print "message sent"
            print "&"*80
            request.session.flash({
                    'class' : 'info',
                    'text'  : u'Письмо для сброса пароля отправлено на %s' % to
                    })
        else:
            request.session.flash({
                    'class' : 'info',
                    'text'  : u'Похоже, что такого адреса у нас нет, попробуйте сначала зарегистрироваться'
                    })
            return HTTPSeeOther(location=request.route_url('login'))

    
    return HTTPSeeOther(location=request.route_url('login'))


def generatetree(req, obj):
    def getparent(obj):
        if obj.parent is not None:
            return getparent(obj.parent)
        else:
            return obj.id

    def jsondata(req, obj):
        strtempl = u"{0} {1} {2} "
        addstr = u"{0} "
        btn_template = u"""<a class="btn btn-sm" style="text-decoration:none;" href="{0}" role="button"> {1}</a>"""
        btn_edit = btn_template.format(req.route_url('courseaction', courseid=getparent(obj), args=('edit', obj.elemtype, obj.id)), u"Править")
        btn_delete = btn_template.format(req.route_url('courseaction', courseid=obj.id, args=('delete', obj.elemtype, obj.id)), u"Удалить")
        btn_add = btn_template.format(req.route_url('courseaction', courseid=obj.id, args=('new', obj.elemtype, obj.id)), u"Добавить")
        nodetext = strtempl.format(obj.trelemtype, obj.childindex, (obj.timetotal if obj.timetotal is not None else '')) 
        #    
        if obj.name is not None:
            nodetext = nodetext + addstr.format(obj.name) 
        if obj.trweektype is not None:
            nodetext = nodetext + addstr.format(obj.trweektype)
        
        #nodetext = nodetext + btn_edit + btn_delete + btn_add
        
        return {"text":nodetext, "href":"#%s"%obj.id, "nodes":[jsondata(req, ch) for ch in obj.children]}

    def jsontree(req, obj):
        outlist = []
        if obj.parent is None:
            for ch in obj.children:
                outlist.append(jsondata(req, ch))
        return json.dumps(outlist, indent=4)
       
    return jsontree(req, obj)


def walkthetree(session, obj, pid, level=1):
    # не работает, сначала надо делать потомков
    # потом родителей
    # иначе получается каша
    # или копировать без потомков, а их отдельно, например
    #


    #newcldrn = []
    parentelement = session.query(CourseElement).filter(CourseElement.id == pid).first()
    if parentelement.elemtype == 'exc':
        session.expunge(obj)
        make_transient(obj)
        obj.id = None
        obj.parent_id = pid
        session.add(obj)
        session.flush()
    else:
        for ch in obj.children:
            session.expunge(ch)
            make_transient(ch)
            ch.id = None
            ch.parent_id = pid
            session.add(ch)
            session.flush()
        
    #session.expunge(obj)
    #make_transient(obj)
    #obj.id = None
    #obj._id = pid
    #obj.children = chldrn
    #session.add(obj)
    #session.flush()

    #for c in obj.children:
    #    walkthetree(session, c, obj.id, level + 1)

def sendmessage(to, what, request, token=None, mess=None):
    fr = 'noreply@manatee.trololo.info'
    replyto = 'a@manatee.trololo.info'
    msg = email.mime.multipart.MIMEMultipart()
    msg['from']=fr
    msg.add_header('reply-to', replyto)
    msg['to']=to
    if what == 'register':
        msg['subject'] = "Регистрация на MakeMeStrong.ru"
        message = """Ура, вы зарегистрировались на makemestrong.ru. 

Для активации учетной записи пройдите по ссылке: {0}

Если это письмо попало к вам по ошибке, просто проигнорируйте эго. 

С уважением, команда MakeMeStrong.
    """
                    
        message = message.format(request.route_url('activateaccount', _query={'login':to, 'token':token}))
    elif what == 'reset':
        msg['subject'] = "Сброс пароля на MakeMeStrong.ru"
        message = """Кто-то воспользовался формой сброса пароля, указав ваш эл. адрес, {0}
Если это были не вы, просто проигнорируйте это сообщение. 

Для сброса пароля пройдите по ссылке: {1}

С уважением, команда MakeMeStrong.
    """
        message = message.format(to, request.route_url('resetpassword', _query={'login':to, 'token':token}))
    elif what == "log":

        msg['subject'] = 'Йуху, что-то происходит на makemestrong'
        message = mess.encode('utf-8')

    msgtext = MIMEText(message, 'plain', 'utf-8')
    msg.attach(msgtext)
    serv = smtplib.SMTP('localhost')
    serv.sendmail(fr, to, msg.as_string())
    serv.quit()
    
def sendemails(subj, message, recipients, request):
    if all(len(a) > 0 for a in [subj, message, recipients]):
        fr = 'noreply@manatee.trololo.info'
        replyto = 'a@manatee.trololo.info'
        mailtemplate = """
<html>
  <head></head>
  <body>
{0}
<p>
С уважением, команда MakeMeStrong.
</p>
 </body>
</html>
"""
        msg = email.mime.multipart.MIMEMultipart()
        msg['from']=fr
        msg.add_header('reply-to', replyto)
        msg['subject'] = subj.encode('utf-8')
        message = mailtemplate.format(message.encode('utf-8'))
        msgtext = MIMEText(message, 'html', 'utf-8')
        msg.attach(msgtext)
        serv = smtplib.SMTP('localhost')
        for to in recipients:
            addr = to.split("|")[0]
            msg['to']=addr
            msgstring = msg.as_string()#.encode('utf-8')
            serv.sendmail(fr, addr, msgstring)
        serv.quit()
    else:
        return False

@view_config(route_name='saveresults')
def saveresultsview(request):
    if not authenticated_userid(request):
        return HTTPSeeOther(location=request.route_url('home'))
    
    if request.POST:
        user = request.dbsession.query(User).filter(User.login == authenticated_userid(request)).first()
        
        maxgrade = request.POST.get('maxgrade')
        bweight = request.POST.get('bodyweight')
        addweighthalf = request.POST.get('addweighthalf')
        gripsizehalf = request.POST.get('gripsizehalf')
        addweightopen = request.POST.get('addweightopen')
        gripsizeopen = request.POST.get('gripsizeopen')
        addweightthree = request.POST.get('addweightthree')
        gripsizethree = request.POST.get('gripsizethree')
        pushups = request.POST.get('pushups')
        pullups = request.POST.get('pullups')

        if user.weight is None:
            user.weight = bweight
            request.dbsession.add(user)
            request.dbsession.flush()

        newresult = Result()
        newresult.maxgrade = maxgrade
        newresult.addweighthalf = addweighthalf
        newresult.gripsizehalf = gripsizehalf
        newresult.addweightopen = addweightopen
        newresult.gripsizeopen = gripsizeopen
        newresult.addweightthree = addweightthree
        newresult.gripsizethree = gripsizethree
        newresult.pushups = pushups
        newresult.pullups = pullups
        newresult.user_id = user.id

        request.dbsession.add(newresult)
        request.session.flash({
                'class' : 'success',
                'text'  : u'Данные сохранены'
                })

        return HTTPSeeOther(location=request.route_url('settings'))


@view_config(route_name='sendemails')
def sendemailsview(request):
    if not authenticated_userid(request):
        return HTTPSeeOther(location=request.route_url('home'))
    else:
        user = request.dbsession.query(User).filter(User.login == authenticated_userid(request)).first()
        if user.is_admin:
            if request.POST:
                message = request.POST.get('mailmessage')
                subject = request.POST.get('mailsubject')
                recipients = request.POST.getall('emails')
                sendemails(subject, message, recipients, request)
                request.session.flash({
                        'class' : 'success',
                        'text'  : u'Сообщения поставлены в очередь на отправку'
                        })
                
    return HTTPSeeOther(location=request.route_url('settings'))



@view_config(route_name='sizes', renderer='../templates/sizestemplate.jinja2')
def sizesview(request):
    tpl = {}
    _here = os.path.dirname(__file__)
    _data = os.path.join(os.path.dirname(_here), 'static', 'data.csv')
    data = pd.read_csv(_data, sep=';')
    data = data.set_index("модель")
    
    ftsize = request.POST.get("footsize", None)
    try:
        ftsize = float(ftsize)
    except:
        return tpl

    if not 20 < ftsize < 31:
        return tpl

    if ftsize is not None:
        yoursize = parsesize(ftsize)
        tpl.update({"yoursize":yoursize})

        newdata = data.apply(parserow, axis=1, size=yoursize)
    
    tpl.update({
            "csvdata":newdata.to_html(na_rep='', border=0, classes=["table", "table-striped", "table-hover"])})
    return tpl

def parsesize(sz):
    # UK size = European size / 1.27 - 25
    # European size = 1.27 × UK size + 31.75
    # EU shoe size = 3/2 x last length (cm) + 1.5 cm
    # UK shoe size = (3 x last length in inches) - 20.5
    # cm to inches 1 cm = 0.393701
    # The Paris point equates to  2⁄3 centimetre (about 6.7 mm or 0.26 in). That is the increment between whole sizes, and about 3.3 mm or 0.13 in between half sizes. This unit is commonly used in Continental Europe.

    # The barleycorn is an old English unit that equates to  1⁄3 inch (8.47 mm). This is the basis for current UK and US shoe sizes, with the largest shoe size taken as twelve inches (a size 12) i.e. 30.5 cm, and then counting backwards in barleycorn units, so a size 11 is 11.67 inches or 29.6 cm.

    # size = (3 * 1.0)/2 * float(sz) + 1.5
    # size = Decimal(size).quantize(0, ROUND_HALF_UP)

    uksizes  = {}
    delta = 0.847*0.5
    maxsize = 30.5
    for x in seq(12, -1, -0.5):
        uksizes[x] = [maxsize-delta, maxsize]
        maxsize = maxsize-delta

    eurosizes = {}
    zerosize=20.0
    delta = 0.67*0.5
    for x in seq(32, 50, 0.5):
        eurosizes[x] = [zerosize, zerosize + delta]
        zerosize = zerosize + delta

    cmsize = float(sz)

    yoursize = [x for x in eurosizes.keys() if eurosizes[x][0]<cmsize<eurosizes[x][1]][-1]
    youruksize = [x for x in uksizes.keys() if uksizes[x][0]<cmsize<uksizes[x][1]][-1]
    return {"EU":yoursize, "CM":cmsize, "UK":youruksize}


def seq(start, stop, step=1):
    n = int(round((stop - start)/float(step)))
    if n > 1:
        return([start + step*i for i in range(n+1)])
    else:
        return([])

def parserow(row, size):
    sizetype = "EU"

    if "UK" in "".join(map(str, row)):
        sizetype = "UK"
    newrow = []

    newrow = []

    for r in row:
        action = 'substr'
        if "larger" in str(r).lower():
            action = 'add'

        if not pd.isnull(r):
            res = re_int.findall(r)
            if len(res) > 0:
                res = res[0]
                if action == 'add':
                    newsize = size[sizetype] + float(res)
                elif action == 'substr':
                    newsize = size[sizetype] - float(res)

                if sizetype == "UK":
                    newsizeUK = newsize
                    newsizeEU = 1.27*newsize+31.75 
                    newsizeEU = Decimal(newsizeEU).quantize(0, ROUND_HALF_UP)
                else:
                    newsizeEU = newsize
                    newsizeUK = newsize/(1.27*1.0)-25
                    newsizeUK = Decimal(newsizeUK).quantize(0, ROUND_HALF_UP)
                    

            else:
                newsizeEU = size['EU']
                newsizeUK = size['UK']
            print row

            newrow.append("{0}EUR / {1}UK".format(newsizeEU, newsizeUK))
        else:
            newrow.append("")

    return newrow
